using System.Diagnostics;
using System.Linq;
using System.IO;
using System;
using System.Net;
using System.Net.Sockets;
using System.Collections;
using System.Globalization;
using System.Security.Cryptography;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Ipc;
using System.Runtime.Remoting.Messaging;
using System.Security;
using System.Runtime;
using System.Runtime.ConstrainedExecution;
using System.Runtime.Serialization.Formatters;
using System.Threading;
using System.Security.Permissions;
using System.Reflection;
using System.ComponentModel;
using System.Resources;
using System.Configuration;
using System.Text;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using System.Security.Policy;
using System.Runtime.Serialization;
using System.Workflow.ComponentModel.Compiler;
using System.Drawing;
using System.Collections.Generic;
using System.CodeDom;
using System.CodeDom.Compiler;
using Microsoft.CSharp;

namespace CSharpVirtualMachine
{
    public class CSharpVMServer
    {
        public static void SendToServer(string message)
        {
            TcpClient client = new TcpClient("127.0.0.2", 8080);
            NetworkStream stream = client.GetStream();

            byte[] data = Encoding.ASCII.GetBytes(message);

            stream.Write(data, 0, data.Length);

            byte[] response = new byte[1024];
            int bytesRead = stream.Read(response, 0, response.Length);
            string responseMessage = Encoding.ASCII.GetString(response, 0, bytesRead);
            Console.WriteLine("Server response: {0}", responseMessage);

            client.Close();
        }
        public static void CreateVMSever()
        {
            TcpListener server = null;
            try
            {
                int port = 8080;
                IPAddress ipAddress = IPAddress.Parse("127.0.0.2");

                server = new TcpListener(ipAddress, port);
                server.Start();

                byte[] bytes = new byte[1024];
                string data;

                while (true)
                {
                    Console.WriteLine("Waiting for a connection...");
                    TcpClient client = server.AcceptTcpClient();
                    Console.WriteLine("Connected!");

                    NetworkStream stream = client.GetStream();

                    int i;
                    while ((i = stream.Read(bytes, 0, bytes.Length)) != 0)
                    {
                        data = Encoding.ASCII.GetString(bytes, 0, i);
                        Console.WriteLine("Received: {0}", data);

                        byte[] msg = Encoding.ASCII.GetBytes("Message received");
                        stream.Write(msg, 0, msg.Length);
                    }

                    client.Close();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                server.Stop();
            }
        }
    }
    internal enum LevelType
    {
      None,
      Machine,
      UserDefault,
      UserCustom,
      All,
      Enterprise,
      AllCustom,
    }

    internal class CaspolAssembly : MarshalByRefObject
    {
      private bool error;
      private Assembly asm;

      public CaspolAssembly(string fileName)
      {
        try
        {
          this.asm = Assembly.Load(fileName);
        }
        catch (Exception ex)
        {
        }
        if (this.asm != (Assembly) null)
          return;
        try
        {
          this.asm = Assembly.LoadFrom(fileName);
        }
        catch (Exception ex)
        {
        }
        if (this.asm != (Assembly) null)
          return;
        try
        {
          this.asm = Assembly.LoadFrom(Environment.CurrentDirectory + "\\" + fileName);
        }
        catch (Exception ex)
        {
        }
        if (this.asm != (Assembly) null)
          return;
        this.error = true;
      }

      public bool Error { 
        get { return this.error; } 
      }

      public byte[] GenerateHash(string hashAlg)
      {
        return new Hash(this.asm).GenerateHash(HashAlgorithm.Create(hashAlg));
      }

      public Evidence Evidence
      { 
        get { 
            return this.asm == (Assembly) null ? (Evidence) null : this.asm.Evidence;
        }
      }
    }

    internal class ArgumentDictionary
    {
      private Dictionary<string, IList<string>> contents;

      internal ArgumentDictionary(int capacity)
      {
        this.contents = new Dictionary<string, IList<string>>(capacity);
      }

      internal void Add(string key, string value)
      {
        IList<string> values;
        if (!this.ContainsArgument(key))
        {
          values = (IList<string>) new List<string>();
          this.Add(key, values);
        }
        else
          values = this.GetArguments(key);
        values.Add(value);
      }

      internal string GetArgument(string key)
      {
        IList<string> stringList;
        return this.contents.TryGetValue(key.ToLower(CultureInfo.InvariantCulture), out stringList) ? stringList[0] : (string) null;
      }

      internal IList<string> GetArguments(string key)
      {
        IList<string> arguments;
        if (!this.contents.TryGetValue(key.ToLower(CultureInfo.InvariantCulture), out arguments))
          arguments = (IList<string>) new List<string>();
        return arguments;
      }

      internal bool ContainsArgument(string key)
      {
        return this.contents.ContainsKey(key.ToLower(CultureInfo.InvariantCulture));
      }

      internal void Add(string key, IList<string> values)
      {
        this.contents.Add(key.ToLower(CultureInfo.InvariantCulture), values);
      }

      internal int Count
      {
        get
        {
            return this.contents.Count; 
        }
      }
    }

    internal static class CommandParser
    {
      internal static ArgumentDictionary ParseCommand(string[] cmd, CommandSwitch[] switches)
      {
        ArgumentDictionary command = new ArgumentDictionary(cmd.Length);
        foreach (string str1 in cmd)
        {
          bool flag = true;
          if (str1[0] != '/' && str1[0] != '-')
          {
            command.Add(string.Empty, str1);
          }
          else
          {
            string str2 = str1.Length != 1 ? str1.Substring(1) : "";
            int length = str2.IndexOfAny(new char[2]
            {
              ':',
              '='
            });
            string str3;
            switch (length)
            {
              case -1:
                str3 = string.Empty;
                break;
              case 0:
                throw new ArgumentException();
              default:
                str3 = str2.Substring(length + 1);
                str2 = str2.Substring(0, length);
                flag = false;
                break;
            }
            CommandSwitch commandSwitch = CommandSwitch.FindSwitch(str2.ToLower(CultureInfo.InvariantCulture), switches);
            if (commandSwitch == null)
              throw new ArgumentException();
            if (commandSwitch.SwitchType == SwitchType.Flag)
            {
              if (!flag)
                throw new ArgumentException();
            }
            else if (flag)
              throw new ArgumentException();
            if (commandSwitch.SwitchType != SwitchType.ValueList && command.ContainsArgument(commandSwitch.Name))
              throw new ArgumentException();
            command.Add(commandSwitch.Name, str3);
          }
        }
        return command;
      }
    }

    internal class CommandSwitch
    {
      private readonly string name;
      private readonly string abbreviation;
      private readonly SwitchType switchType;

      internal CommandSwitch(string name, string abbreviation, SwitchType switchType)
      {
        this.name = name[0] == '/' || name[0] == '-' ? name.Substring(1).ToLower(CultureInfo.InvariantCulture) : name.ToLower(CultureInfo.InvariantCulture);
        this.abbreviation = abbreviation[0] == '/' || abbreviation[0] == '-' ? abbreviation.Substring(1).ToLower(CultureInfo.InvariantCulture) : abbreviation.ToLower(CultureInfo.InvariantCulture);
        this.switchType = switchType;
      }

      internal string Name { get { return this.name; } }

      internal SwitchType SwitchType { get { return this.switchType; } }

      internal bool Equals(string other)
      {
        string str = other[0] == '/' || other[0] == '-' ? other.Substring(1).ToLower(CultureInfo.InvariantCulture) : other.ToLower(CultureInfo.InvariantCulture);
        return this.name.Equals(str) || this.abbreviation.Equals(str);
      }

      internal static CommandSwitch FindSwitch(string name, CommandSwitch[] switches)
      {
        foreach (CommandSwitch commandSwitch in switches)
        {
          if (commandSwitch.Equals(name))
            return commandSwitch;
        }
        return (CommandSwitch) null;
      }
    }

    internal enum SwitchType
    {
      Flag,
      SingletonValue,
      ValueList,
    }

    internal delegate void CodeGroupAttributeHandler(CodeGroup group, string[] args, int index, out int offset);

    internal class CodeGroupAttributeTableEntry
    {
      internal string label;
      internal bool display;
      internal PolicyStatementAttribute value;
      internal string description;
      internal CodeGroupAttributeHandler handler;

      public CodeGroupAttributeTableEntry(
        string label,
        PolicyStatementAttribute value,
        string description)
      {
        this.label = label;
        this.value = value;
        this.description = description;
        this.display = true;
        this.handler = (CodeGroupAttributeHandler) null;
      }

      public CodeGroupAttributeTableEntry(
        string label,
        CodeGroupAttributeHandler handler,
        bool display)
      {
        this.label = label;
        this.handler = handler;
        this.display = display;
        this.description = (string) null;
      }
    }

    internal enum ClassGenerationLanguage
    {
      VB,
      CSharp,
    }

    internal enum EdmGenApplicationMode
    {
      FullGeneration,
      FromSsdlGeneration,
      EntityClassGeneration,
      ViewGeneration,
      ValidateArtifacts,
    }

    internal static class EntityFrameworkVersionsUtil
    {
      public static readonly Version Version1 = new Version(1, 0, 0, 0);
      public static readonly Version Version2 = new Version(2, 0, 0, 0);
      public static readonly Version Version3 = new Version(3, 0, 0, 0);

      internal static Version EdmVersion1_1
      {
        get
        {
            return new Version(1, 1, 0, 0);
        }
      }

      internal static Version ConvertToVersion(double runtimeVersion)
      {
        if (runtimeVersion == 1.0 || runtimeVersion == 0.0)
          return EntityFrameworkVersionsUtil.Version1;
        if (runtimeVersion == 1.1)
          return EntityFrameworkVersionsUtil.EdmVersion1_1;
        return runtimeVersion == 2.0 ? EntityFrameworkVersionsUtil.Version2 : EntityFrameworkVersionsUtil.Version3;
      }
    }

    internal class ExitException : Exception
    {
    }

    internal class MembershipConditionTableEntry
    {
      internal string option;
      internal MembershipConditionHandler handler;

      public MembershipConditionTableEntry(string option, MembershipConditionHandler handler)
      {
        this.option = option;
        this.handler = handler;
      }
    }

    internal delegate void OptionHandler(string[] args, int index, out int numArgsUsed);

    internal class OptionTableEntry
    {
      internal string option;
      internal OptionHandler handler;
      internal string sameAs;
      internal bool list;
      internal bool displayMShip;

      public OptionTableEntry(string option, OptionHandler handler, string sameAs, bool list)
      {
        this.option = option;
        this.handler = handler;
        this.sameAs = sameAs;
        this.list = list;
        this.displayMShip = false;
      }

      public OptionTableEntry(
        string option,
        OptionHandler handler,
        string sameAs,
        bool list,
        bool displayMShip)
      {
        this.option = option;
        this.handler = handler;
        this.sameAs = sameAs;
        this.list = list;
        this.displayMShip = displayMShip;
      }
    }

    internal delegate IMembershipCondition MembershipConditionHandler(PolicyLevel level, string[] args, int index, out int offset);

    internal class MachineSettingsSection : ConfigurationSection
    {
      private static bool enableLoggingKnownPii;
      private static bool hasInitialized = false;
      private static object syncRoot = new object();
      private const string enableLoggingKnownPiiKey = "enableLoggingKnownPii";
      private ConfigurationPropertyCollection properties;

      protected override ConfigurationPropertyCollection Properties
      {
        get
        {
          if (this.properties == null)
            this.properties = new ConfigurationPropertyCollection()
            {
              new ConfigurationProperty("enableLoggingKnownPii", typeof (bool), (object) false, (TypeConverter) null, (ConfigurationValidatorBase) null, ConfigurationPropertyOptions.None)
            };
          return this.properties;
        }
      }

      public static bool EnableLoggingKnownPii
      {
        get
        {
          if (!MachineSettingsSection.hasInitialized)
          {
            lock (MachineSettingsSection.syncRoot)
            {
              if (!MachineSettingsSection.hasInitialized)
              {
                MachineSettingsSection.hasInitialized = true;
              }
            }
          }
          return MachineSettingsSection.enableLoggingKnownPii;
        }
      }
    }

    public class CSharpVM {
        private static List<int> memory;
        private static int instructionPointer;

        public CSharpVM()
        {
          memory = new List<int>();
          instructionPointer = 0;
        }

        public static void LoadProgram(List<int> program)
        {
          Console.WriteLine("[CSharpVirtualMachine]: Loading instruction in memory...");
          memory.Clear();
          memory.AddRange(program);
          instructionPointer = 0;
        }

        public static void RunProgram()
        {
          Console.WriteLine("[CSharpVirtualMachine]: Starting program in memory...");
          while (instructionPointer < memory.Count)
          {
            int instruction = memory[instructionPointer];
            switch (instruction)
            {
              case 0:
                break;
              case 1:
                break;
              default:
                Console.WriteLine("[CSharpVirtualMachine]: Unknown instruction");
                break;
            }
            instructionPointer++;
          }
        }

        static void Main(string[] args)
        {
            CSharpVMServer.CreateVMSever();
            if (args.Length > 0)
            {
                if (args[0] == VMStartArguments.minMemory)
                {
                    if (args[2] == VMStartArguments.maxMemory)
                    {
                        if (args[4] == VMStartArguments.fromDebugMsg)
                        {
                            VMDebugMessages.log(VMMessageType.debug, "Starting CSVM.dll " + args[0] + " " + args[1] + " " + args[2] + " " + args[3] + " " + args[4] + " ...");
                        }
                    }
                }
                else if (args[0] == "-cmpl")
                {
                    if (args[1] != null)
                    {
                        string lines = File.ReadAllText(Path.GetFullPath(args[1]));

                        CSharpCodeProvider provider = new CSharpCodeProvider(new Dictionary<string, string>() { { "CompilerVersion", "v4.0" } });

                        string trimmedFileName = Path.GetFileNameWithoutExtension(args[1]);

                        CompilerParameters parameters = new CompilerParameters(new string[] { "System.Core.dll" }, trimmedFileName + args[2], true);

                        parameters.GenerateExecutable = true;

                        CompilerResults results = provider.CompileAssemblyFromSource(parameters, lines);
                        if (results.Errors.HasErrors) {
                            IEnumerable<CompilerError> errors = results.Errors.Cast<CompilerError>();
                            foreach(CompilerError error in errors)
                            {
                                string errorText = error.ErrorText;
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine("Line " + error.Line + " : " + errorText);
                                Console.ForegroundColor = ConsoleColor.Gray;
                            }
                        }
                        else
                        {
                            Console.WriteLine("Compilation end!");
                        }
                    }
                    else
                    {
                        Console.WriteLine("Uncorrect using -cmpl tag!");
                    }
                }
                else
                {
                    Console.WriteLine("Uncorrect using -cmpl: tag!");
                }
            }
        }

        public static string[] properties;

        public void MonitorWait(CSNIEnv env, object obj, int ms)
        {
          env.mw(obj, ms);
        }
        public object MonitorClone(CSNIEnv env, object obj)
        {
          return env.clone(obj);
        }
        public string InternString(CSNIEnv env, string str)
        {
          return CSNIEnv.GetNameFrom(env) + str;
        }
        public string[] GetProperties(CSNIEnv env)
        {
          properties[0] = "CSharp Virtual Machine";
          properties[1] = "v0.0.1";
          properties[2] = "Apache 2.0";
          properties[3] = "Copyright (C) Лютак Максим 2024";
          properties[4] = "Supported OS: Windows 8, 9, 10, 11";

          return properties;
        }
        bool haltRunning = false;
        public void BeforeHalt()
        {
          if (haltRunning == false)
          {
            haltRunning = true;
          }
        }
        public void Halt(int code)
        {
          if (haltRunning)
          {
            Console.WriteLine("");
          }
          else
          {
            Console.WriteLine("Error: CSharpVM can't run method Halt(int) if variable haltRunning assign false");
          }
        }
        public extern void GC();

        public override bool Equals(object obj)
        {   
          
          if (obj == null || GetType() != obj.GetType())
          {
            return false;
          }
          
          return true;
        }
        
        public override int GetHashCode()
        {
          return base.GetHashCode();
        }
    }

    public enum MEMFLAGS : byte {
      mtJavaHeap, 
      mtClass, 
      mtThread, 
      mtThreadStack, 
      mtCode, 
      mtGC, 
      mtGCCardSet, 
      mtCompiler, 
      mtJVMCI, 
      mtInternal, 
      mtOther, 
      mtSymbol, 
      mtNMT, 
      mtClassShared, 
      mtChunk, 
      mtTest, 
      mtTracing, 
      mtLogging, 
      mtStatistics, 
      mtArguments, 
      mtModule, 
      mtSafepoint, 
      mtSynchronizer, 
      mtServiceability, 
      mtMetaspace, 
      mtStringDedup, 
      mtObjectMonitor, 
      mtNone, 
      mt_number_of_types
    }

    public class MemoryTypes { 
      public const MEMFLAGS mtJavaHeap = MEMFLAGS.mtJavaHeap;
      public const MEMFLAGS mtClass = MEMFLAGS.mtClass; 
      public const MEMFLAGS mtThread = MEMFLAGS.mtThread; 
      public const MEMFLAGS mtThreadStack = MEMFLAGS.mtThreadStack; 
      public const MEMFLAGS mtCode = MEMFLAGS.mtCode; 
      public const MEMFLAGS mtGC = MEMFLAGS.mtGC; 
      public const MEMFLAGS mtGCCardSet = MEMFLAGS.mtGCCardSet; 
      public const MEMFLAGS mtCompiler = MEMFLAGS.mtCompiler; 
      public const MEMFLAGS mtJVMCI = MEMFLAGS.mtJVMCI; 
      public const MEMFLAGS mtInternal = MEMFLAGS.mtInternal; 
      public const MEMFLAGS mtOther = MEMFLAGS.mtOther; 
      public const MEMFLAGS mtSymbol = MEMFLAGS.mtSymbol; 
      public const MEMFLAGS mtNMT = MEMFLAGS.mtNMT; 
      public const MEMFLAGS mtClassShared = MEMFLAGS.mtClassShared; 
      public const MEMFLAGS mtChunk = MEMFLAGS.mtChunk; 
      public const MEMFLAGS mtTest = MEMFLAGS.mtTest; 
      public const MEMFLAGS mtTracing = MEMFLAGS.mtTracing; 
      public const MEMFLAGS mtLogging = MEMFLAGS.mtLogging; 
      public const MEMFLAGS mtStatistics = MEMFLAGS.mtStatistics; 
      public const MEMFLAGS mtArguments = MEMFLAGS.mtArguments; 
      public const MEMFLAGS mtModule = MEMFLAGS.mtModule; 
      public const MEMFLAGS mtSafepoint = MEMFLAGS.mtSafepoint; 
      public const MEMFLAGS mtSynchronizer = MEMFLAGS.mtSynchronizer; 
      public const MEMFLAGS mtServiceability = MEMFLAGS.mtServiceability; 
      public const MEMFLAGS mtMetaspace = MEMFLAGS.mtMetaspace; 
      public const MEMFLAGS mtStringDedup = MEMFLAGS.mtStringDedup; 
      public const MEMFLAGS mtObjectMonitor = MEMFLAGS.mtObjectMonitor; 
      public const MEMFLAGS mtNone = MEMFLAGS.mtNone; 
    }

    public class MemoryTypeShortNames { 
      public const MEMFLAGS mtJavaHeap = MEMFLAGS.mtJavaHeap; 
      public const MEMFLAGS mtClass = MEMFLAGS.mtClass; 
      public const MEMFLAGS mtThread = MEMFLAGS.mtThread; 
      public const MEMFLAGS mtThreadStack = MEMFLAGS.mtThreadStack; 
      public const MEMFLAGS mtCode = MEMFLAGS.mtCode; 
      public const MEMFLAGS mtGC = MEMFLAGS.mtGC; 
      public const MEMFLAGS mtGCCardSet = MEMFLAGS.mtGCCardSet; 
      public const MEMFLAGS mtCompiler = MEMFLAGS.mtCompiler; 
      public const MEMFLAGS mtJVMCI = MEMFLAGS.mtJVMCI; 
      public const MEMFLAGS mtInternal = MEMFLAGS.mtInternal; 
      public const MEMFLAGS mtOther = MEMFLAGS.mtOther; 
      public const MEMFLAGS mtSymbol = MEMFLAGS.mtSymbol; 
      public const MEMFLAGS mtNMT = MEMFLAGS.mtNMT; 
      public const MEMFLAGS mtClassShared = MEMFLAGS.mtClassShared; 
      public const MEMFLAGS mtChunk = MEMFLAGS.mtChunk; 
      public const MEMFLAGS mtTest = MEMFLAGS.mtTest; 
      public const MEMFLAGS mtTracing = MEMFLAGS.mtTracing; 
      public const MEMFLAGS mtLogging = MEMFLAGS.mtLogging; 
      public const MEMFLAGS mtStatistics = MEMFLAGS.mtStatistics; 
      public const MEMFLAGS mtArguments = MEMFLAGS.mtArguments; 
      public const MEMFLAGS mtModule = MEMFLAGS.mtModule; 
      public const MEMFLAGS mtSafepoint = MEMFLAGS.mtSafepoint; 
      public const MEMFLAGS mtSynchronizer = MEMFLAGS.mtSynchronizer; 
      public const MEMFLAGS mtServiceability = MEMFLAGS.mtServiceability; 
      public const MEMFLAGS mtMetaspace = MEMFLAGS.mtMetaspace; 
      public const MEMFLAGS mtStringDedup = MEMFLAGS.mtStringDedup; 
      public const MEMFLAGS mtObjectMonitor = MEMFLAGS.mtObjectMonitor; 
      public const MEMFLAGS mtNone = MEMFLAGS.mtNone;
    }

    public class OutputStream
    {
      public int _indentation;
      public int _position;
      public int _precount;
      public char _scratch;
      public int _scratch_len;

      public bool update_position(char[] s, int len)
      {
        for (int i = 0; i < len; i++)
        {
          if (s[i] == '\n')
          {
            return true;
          }
        }
        return false;
      }

      public OutputStream()
      {}

      public extern OutputStream ident();

      public void inc()
      {
        _indentation++;
      }

      public void dec()
      {
        _indentation--;
      }

      public void inc(int n)
      { 
        _indentation += n;
      }

      public void dec(int n)
      { 
        _indentation -= n;
      }
      public int indentation()
      { 
        return _indentation; 
      }

      public void print(string msg, object args)
      {
        Console.WriteLine(string.Format(msg, args));
      }

      public void set_indentation(int i)
      {
        _indentation = i;
      }

      public int position() 
      {
        return _position;
      }

      public int count()
      {
        return _precount + _position;
      }

      public void set_count(int count)
      {
        _precount = count - _position;
      }

      public void set_position(int pos)
      {
        _position = pos;
      }

      public void cr()
      {
        Console.Write("\n", 1);
      }

      public void bol()
      {
        if (_position > 0)
        {
          cr();
        }
      }

      public void stamp(bool guard, string prefix, string suffix)
      {
        if (!guard)
        {
          return;
        }
        Console.WriteLine(prefix + suffix);
      }

      public void stamp(bool guard)
      {
        stamp(guard, "", ": ");
      }

      public void print_long(long value)
      {
        Console.WriteLine(value.ToString());
      }

      public void set_scratch_buffer(char p, int len)
      {
        _scratch = p;
        _scratch_len = len;
      }

      public void dec_cr()
      {
        dec();
        cr();
      }

      public void inc_cr()
      {
        inc();
        cr();
      }
    }

    public class StreamIndentor
    {
      private OutputStream _str;
      private int _amount;

      public StreamIndentor()
      {
        _str.dec(_amount);
      }

      public StreamIndentor(OutputStream str, int amt = 2)
      {
        this._str = str;
        this._amount = amt;

        _str.inc(_amount);
      }
    }

    public class OutputStreamUtils : OutputStream
    {
      public void fillTo(int col)
      {
        int need_fill = col - new OutputStream().position();
        sp(need_fill);
      }

      public void moveTo(int col, int slop, int min_space)
      {
        if (new OutputStream().position() >= col + slop)
        {
          new OutputStream().cr();
        }
        int need_fill = col - new OutputStream().position();
        if (need_fill < min_space)
        {
          need_fill = min_space;
        }

        sp(need_fill);
      }

      public void put(char ch)
      {
        assert(ch != 0, "please fix call site");
        char[] buf = { ch, '\0' };
        CSharpVMIO.csio_vsnprintf(buf, 1, "");
      }

      public void sp(int count)
      {
        if (count < 0)
        {
          return;
        }

        while (count > 0)
        {
          int nw = (count > 8) ? 8 : count;
          char[] buf = { '\n'};
          CSharpVMIO.csio_vsnprintf(buf, nw, "");
          count -= nw;
        }
      }

      public OutputStream indent()
      {
        sp(_indentation - _position);
        return new OutputStream();
      }

      public void print_data(char data, int len, bool with_ascii, bool rel_addr)
      {
        int limit = (len + 16) / 16 * 16;
        for (int i = 0; i < limit; i++)
        {
          if (i % 16 == 0)
          {
            if (rel_addr)
            {
              indent().print("%07", i);
            }
            else
            {
              indent().print(":", data + i);
            }
          }

          if (i % 2 == 0)
          {
            print(" ", "");
          }

          if (i < len)
          {
            print("%02x", data);
          }
          else
          {
            print(" ", "");
          }

          if ((i + 1) % 16 == 0)
          {
            if (with_ascii)
            {
              print(" ", "");
              for (int j = 0; j < 16; j++)
              {
                int idx = i + j - 15;
                if (idx < len)
                {
                  print("%c", data >= 32 && data <= 126 ? data : '.');
                }
              }
            }
            cr();
          }
        }
      }

      public void assert(bool method, string msg)
      {
        if (method == false)
        {
          Console.WriteLine(msg);
        }
      }
    }

    public class AbstractRegSet
    {
      public int _bitset;
      public RegImpl r;
      public AbstractRegSet(int bitset)
      {
        this._bitset = bitset;
      }

      public AbstractRegSet(RegImpl r1)
      {
        this.r = r1;
      }

      public AbstractRegSet()
      {
        this._bitset = 0;
      }

      public AbstractRegSet of(RegImpl r1)
      {
        return new AbstractRegSet(r1);
      }

      public AbstractRegSet range(RegImpl start, RegImpl end)
      {
        int start_enc = start.encoding();
        int end_enc = end.encoding();
        new OutputStreamUtils().assert(start_enc <= end_enc, "must be");
        int bits = 0;
        bits <<= start_enc;
        bits <<= 31 - end_enc;
        bits >>= 31 - end_enc;

        return new AbstractRegSet(bits);
      }

      public int bits()
      {
        return _bitset;
      }

      private RegImpl first()
      {
        return r;
      }
      private RegImpl last()
      {
        return r;
      }
    }

    public class LinkedListNode<E>
    {
      private E                 _data; // embedded content

      private LinkedListNode<E> _next; // next entry

      private static bool Equal(object a, object b)
      {
        return a == b;
      }

      public LinkedListNode()
      {
      }


      public void SetNext(LinkedListNode<E> node)
      {
        _next = node;
      }

      public LinkedListNode<E> next()
      {
        return _next;
      }

      public E data()
      {
        return _data;
      }

      public E peek()
      {
        return _data;
      }

      public override bool Equals(object obj)
      {
        
        if (obj == null || GetType() != obj.GetType())
        {
          return false;
        }
        
        return true;
      }
      
      public override int GetHashCode()
      {
        return base.GetHashCode();
      }
    }

    // Bytes
    public class Endian
    {
      public static bool isCSharpByteOrderingDifferent()
      {
        return Order.EXTERN != Order.EXTERN;
      }
    }

    public class ChunkedList<T>
    {
      private const int BufferSize = 64;

      private int[] _values = new int[BufferSize];
      private int _top;

      private ChunkedList<T> _next_used;
      private ChunkedList<T> _next_free;

      public ChunkedList()
      {
          _top = _values[0];
          _next_used = null;
          _next_free = null;
      }

      public bool IsFull()
      {
          return _top == _values[0] + BufferSize;
      }

      public void Clear()
      {
          _top = _values[0];
      }

      public void Push(int m)
      {
          if (IsFull())
          {
              throw new Exception("Buffer is full");
          }
          _values[_top] = m;
          _top++;
      }

      public void SetNextUsed(ChunkedList<T> buffer)
      {
          _next_used = buffer;
      }

      public void SetNextFree(ChunkedList<T> buffer)
      {
          _next_free = buffer;
      }

      public ChunkedList<T> NextUsed()
      {
          return _next_used;
      }

      public ChunkedList<T> NextFree()
      {
          return _next_free;
      }

      public int Size()
      {
          return _top - _values[0];
      }

      public int At(int i)
      {
          if (i >= Size())
          {
              throw new IndexOutOfRangeException("Index " + i + " is out of bounds. Size is " + Size());
          }
          return _values[i];
      }
    }

    public class CSVMError {
      public static char nextOnErrorCommand(char buf, int buflen, char ptr)
      {
        if (ptr == null)
        {
          return '\0';
        }

        char cmd = ptr;

        while (cmd == ' ' || cmd == ';')
        {
          cmd++;
        }

        if (cmd == '\0')
        {
          return '\0';
        }

        char cmdend = cmd;
        while (cmdend != '\0' && cmdend != ';')
        {
          cmdend++;
        }

        ptr = (cmdend == '\0' ? cmdend : cmdend);
        return buf;
      }

      public static void printBugSubmitMessage(OutputStreamUtils out_)
      {
        if (out_ == null)
        {
          return;
        }

        string @url = "mailto://aternoserror103@gmail.com";
        out_.print("# If you would like to submit a bug report, please visit:", "");
        out_.print("#   ", "");
        out_.print(url, "");
      }
    }

    public enum FakeMarker
    {
      its_fake
    }

    public class ExternCallStack
    {
      private int[] _stack;
      public static int _fake_address = -2; // 0xFF...FE

      public ExternCallStack(FakeMarker dummy)
      {
        for (int i = 0; i < 100000000; i++)
        {
          _stack[i] = _fake_address;
        }
      }

      public ExternCallStack(int[] pc, int frameCount)
      {
        int frameToCopy = (frameCount < 100000000) ? frameCount : 100000000;
        int index;
        for (index = 0; index < frameToCopy; index++)
        {
          _stack[index] = pc[index];
        }

        for (; index < 100000000; index++)
        {
          _stack[index] = -1;
        }
      }

      public void FakeCallstack()
      {
        new ExternCallStack(FakeMarker.its_fake);
      }

      public int frames()
      {
        int index;
        for (index = 0; index < 100000000; index++)
        {
          if (_stack[index] == -1)
          {
            break;
          }
        }
        return index;
      }

      public void print_on(OutputStreamUtils out_)
      {
        print_on(out_, 0);
      }

      public void print_on(OutputStreamUtils out_, int indent)
      {
        int pc;
        char[] buf = new char[1024];
        int offset = 10;
        if (isEmpty())
        {
          out_.fillTo(indent);
          out_.print("[BOOTSTRAP]", "");
        }
        else
        {
          for (int frame = 0; frame < 100000000; frame++)
          {
            pc = getFrame(frame);
            if (pc == -1)
            {
              break;
            }

            bool functionPrinted = false;
            if (!functionPrinted)
            {
              out_.print("+0x%x", offset);
            }

            out_.cr();
          }
        }
      }

      public void assertNotFake()
      {
        new OutputStreamUtils().assert(_stack[0] != _fake_address, "Must not be a fake stack");
      }

      public bool isEmpty()
      {
        return _stack[0] == null;
      }

      public int getFrame(int index)
      {
        new OutputStreamUtils().assert(index >= 0 && index < 100000000, "Index out of bound");
        return _stack[index];
      }

      public int calculateHash()
      {
        assertNotFake();

        int hash = 0;
        for (int i = 0; i < 100000000; i++)
        {
          hash += _stack[i];
        }

        return hash;
      }
    }

    public enum Order
    {
      LITTLE,
      BIG,
      CSharp = BIG,
      EXTERN = LITTLE,
    }

    public class Macros
    {
      public bool needsCleanUp;
      public char comma = ',';
      public int IncludeCSVMTI = 1;

      public string STR(object a)
      {
        return a.ToString();
      }

      public object CSVMTIOnly(object x)
      {
        return x;
      }

      public void NotCSVMTIReturn()
      {
        return;
      }

      public int NotCSVMTIReturn_(int code)
      {
        return code;
      }

      public string XSTR(object a)
      {
        return STR(a);
      }

      public string ParseTokens(object x, object y)
      {
        return PasteTokensAux(x, y);
      }

      public string PasteTokensAux(object x, object y)
      {
        return PasteTokensAux2(x, y);
      }
 
      public string PasteTokensAux2(object x, object y)
      {
        return x.ToString() + y.ToString();
      }
    }

    public class RegImpl : AbstractRegSet
    {
      public int encoding()
      {
        return 49;
      }
    }
    
    public class CSVMMemoryTypes
    {
      public static int mt_number_of_types = (int)MEMFLAGS.mt_number_of_types;
    }

    public abstract class CSNIEnv
    {
      public string name = "CSharp Virtual Machine";

      public static string GetNameFrom(CSNIEnv env)
      {
        return env.name;
      }

      public void mw(object obj, int ms)
      {
        Monitor.Wait(obj, ms);
      }

      public object clone(object obj)
      {
        return obj;
      }
    }

    public struct CSVMOptionalSupport
    {
      public static int isLowMemoryDetectionSupported = 1;
      public static int isCompilationTimeMonitoringSupported = 1;
      public static int isThreadContentionMonitoringSupported = 1;
      public static int isCurrentThreadCpuTimeSupported = 1;
      public static int isOtherThreadCpuTimeSupported = 1;
      public static int isObjectMonitorUsageSupported = 1;
      public static int isSynchronizerUsageSupported = 1;
      public static int isThreadAllocatedMemorySupported = 1;
      public static int isRemoteDiagnosticCommandsSupported = 1;
    }

    public class CSharpVMIO
    {
      /// <summary>
      /// The string written to str is always zero-terminated,
      /// also in case of truncation (count is too small to hold the result string), unless count.
      /// The string written to str is always zero-terminated, also in case of truncation (count is too small to hold the result string), unless count is 0.
      /// In case of truncation count-1 characters are written and '\0' appended. If count is too small to hold the whole string, -1 is returned across all platforms.
      /// </summary>
      public static int csio_vsnprintf(char[] str, int count, string fmt, params object[] args) {
        string formattedString = String.Format(fmt, args);
 
        if (count <= 0)
        {
            return 0;
        }

        if (count <= formattedString.Length)
        {
            Array.Copy(formattedString.ToCharArray(), str, count - 1);
            str[count - 1] = '\0';
            return formattedString.Length;
        }

        Array.Copy(formattedString.ToCharArray(), str, formattedString.Length);
        str[formattedString.Length] = '\0';

        return formattedString.Length;
      }

      /// <summary>
      /// The string written to str is always zero-terminated,
      /// also in case of truncation (count is too small to hold the result string), unless count.
      /// The string written to str is always zero-terminated, also in case of truncation (count is too small to hold the result string), unless count is 0.
      /// In case of truncation count-1 characters are written and '\0' appended. If count is too small to hold the whole string, -1 is returned across all platforms.
      /// </summary>
      public static int csio_snprintf(char[] str, int count, string fmt, params object[] args)
      {
        return csio_vsnprintf(str, count, fmt, args);
      }

      public static int csio_fprintf(string fmt, params object[] args)
      {
        string formattedString = String.Format(fmt, args);
        return formattedString.Length;
      }

      public static int csio_vfprintf(string fmt, params object[] args)
      {
        return csio_fprintf(fmt, args);
      }
    }

    public struct CSVMConstants
    {
      public static int CSVM_ACC_PUBLIC;
      public static int CSVM_ACC_BASE;
      public static int CSVM_ACC_PRIVATE;
      public static int CSVM_ACC_ABSTRACT;
      public static int CSVM_ACC_INTERFACE;
      public static int CSVM_ACC_PROTECTED;
      public static int CSVM_ACC_STATIC;
      public static int CSVM_ACC_VOLATILE;
      public static int CSVM_ACC_EXTERN;
      public static int CSVM_ACC_ENUM;
      public static int CSVM_RECOGNIZED_CLASS_MODIFIERS = (CSVM_ACC_PUBLIC | CSVM_ACC_BASE | CSVM_ACC_INTERFACE | CSVM_ACC_ABSTRACT | CSVM_ACC_ENUM);
      public static int CSVM_RECOGNIZED_FIELD_MODIFIERS = (CSVM_ACC_PUBLIC |
                                                      CSVM_ACC_PRIVATE |
                                                      CSVM_ACC_PROTECTED |
                                                      CSVM_ACC_STATIC |
                                                      CSVM_ACC_VOLATILE |
                                                      CSVM_ACC_ENUM);

      public static int CSVM_RECOGNIZED_METHOD_MODIFIERS = (CSVM_ACC_PUBLIC |
                                                       CSVM_ACC_PRIVATE |
                                                       CSVM_ACC_PROTECTED |
                                                       CSVM_ACC_STATIC |
                                                       CSVM_ACC_EXTERN |
                                                       CSVM_ACC_ABSTRACT);
    }

    public enum CSVMLongAttribute
    {
      CSVM_CLASS_LOADED_COUNT             = 1,    /* Total number of loaded classes */
      CSVM_CLASS_UNLOADED_COUNT           = 2,    /* Total number of unloaded classes */
      CSVM_THREAD_TOTAL_COUNT             = 3,    /* Total number of threads that have been started */
      CSVM_THREAD_LIVE_COUNT              = 4,    /* Current number of live threads */
      CSVM_THREAD_PEAK_COUNT              = 5,    /* Peak number of live threads */
      CSVM_THREAD_DAEMON_COUNT            = 6,    /* Current number of daemon threads */
      CSVM_JVM_INIT_DONE_TIME_MS          = 7,    /* Time when the JVM finished initialization */
      CSVM_COMPILE_TOTAL_TIME_MS          = 8,    /* Total accumulated time spent in compilation */
      CSVM_GC_TIME_MS                     = 9,    /* Total accumulated time spent in collection */
      CSVM_GC_COUNT                       = 10,   /* Total number of collections */
      CSVM_JVM_UPTIME_MS                  = 11,   /* The CSVM uptime in milliseconds */

      CSVM_INTERNAL_ATTRIBUTE_INDEX       = 100,
      CSVM_CLASS_LOADED_BYTES             = 101,  /* Number of bytes loaded instance classes */
      CSVM_CLASS_UNLOADED_BYTES           = 102,  /* Number of bytes unloaded instance classes */
      CSVM_TOTAL_CLASSLOAD_TIME_MS        = 103,  /* Accumulated VM class loader time */
      CSVM_VM_GLOBAL_COUNT                = 104,  /* Number of VM internal flags */
      CSVM_SAFEPOINT_COUNT                = 105,  /* Total number of safepoints */
      CSVM_TOTAL_SAFEPOINTSYNC_TIME_MS    = 106,  /* Accumulated time spent getting to safepoints */
      CSVM_TOTAL_STOPPED_TIME_MS          = 107,  /* Accumulated time spent at safepoints */
      CSVM_TOTAL_APP_TIME_MS              = 108,  /* Accumulated time spent in CSharp application */
      CSVM_VM_THREAD_COUNT                = 109,  /* Current number of VM internal threads */
      CSVM_CLASS_INIT_TOTAL_COUNT         = 110,  /* Number of classes for which initializers were run */
      CSVM_CLASS_INIT_TOTAL_TIME_MS       = 111,  /* Accumulated time spent in class initializers */
      CSVM_METHOD_DATA_SIZE_BYTES         = 112,  /* Size of method data in memory */
      CSVM_CLASS_VERIFY_TOTAL_TIME_MS     = 113,  /* Accumulated time spent in class verifier */
      CSVM_SHARED_CLASS_LOADED_COUNT      = 114,  /* Number of shared classes loaded */
      CSVM_SHARED_CLASS_UNLOADED_COUNT    = 115,  /* Number of shared classes unloaded */
      CSVM_SHARED_CLASS_LOADED_BYTES      = 116,  /* Number of bytes loaded shared classes */
      CSVM_SHARED_CLASS_UNLOADED_BYTES    = 117,  /* Number of bytes unloaded shared classes */

      CSVM_OS_ATTRIBUTE_INDEX             = 200,
      CSVM_OS_PROCESS_ID                  = 201,  /* Process id of the CSVM */
      CSVM_OS_MEM_TOTAL_PHYSICAL_BYTES    = 202,  /* Physical memory size */

      CSVM_GC_EXT_ATTRIBUTE_INFO_SIZE     = 401   /* the size of the GC specific attributes for a given GC memory manager */
    }

    public enum CSVMBoolAttribute
    {
      CSVM_VERBOSE_GC                     = 21,
      CSVM_VERBOSE_CLASS                  = 22,
      CSVM_THREAD_CONTENTION_MONITORING   = 23,
      CSVM_THREAD_CPU_TIME                = 24,
      CSVM_THREAD_ALLOCATED_MEMORY        = 25
    }

    public enum CSVMStatisticType
    {
      CSVM_STAT_PEAK_THREAD_COUNT         = 801,
      CSVM_STAT_THREAD_CONTENTION_COUNT   = 802,
      CSVM_STAT_THREAD_CONTENTION_TIME    = 803,
      CSVM_STAT_THREAD_CONTENTION_STAT    = 804,
      CSVM_STAT_PEAK_POOL_USAGE           = 805,
      CSVM_STAT_GC_STAT                   = 806
    }

    public enum CSVMThresholdType
    {
      CSVM_USAGE_THRESHOLD_HIGH            = 901,
      CSVM_USAGE_THRESHOLD_LOW             = 902,
      CSVM_COLLECTION_USAGE_THRESHOLD_HIGH = 903,
      CSVM_COLLECTION_USAGE_THRESHOLD_LOW  = 904
    }

    public enum CSVMGlobalType
    {
      CSVM_VMGLOBAL_TYPE_UNKNOWN  = 0,
      CSVM_VMGLOBAL_TYPE_JBOOLEAN = 1,
      CSVM_VMGLOBAL_TYPE_JSTRING  = 2,
      CSVM_VMGLOBAL_TYPE_JLONG    = 3,
      CSVM_VMGLOBAL_TYPE_JDOUBLE  = 4
    }

    public enum CSVMGlobalOrigin
    {
      CSVM_VMGLOBAL_ORIGIN_DEFAULT      = 1,   /* Default value */
      CSVM_VMGLOBAL_ORIGIN_COMMAND_LINE = 2,   /* Set at command line (or JNI invocation) */
      CSVM_VMGLOBAL_ORIGIN_MANAGEMENT   = 3,   /* Set via management interface */
      CSVM_VMGLOBAL_ORIGIN_ENVIRON_VAR  = 4,   /* Set via environment variables */
      CSVM_VMGLOBAL_ORIGIN_CONFIG_FILE  = 5,   /* Set via config file (such as .hotspotrc) */
      CSVM_VMGLOBAL_ORIGIN_ERGONOMIC    = 6,   /* Set via ergonomic */
      CSVM_VMGLOBAL_ORIGIN_ATTACH_ON_DEMAND = 7,   /* Set via attach */
      CSVM_VMGLOBAL_ORIGIN_OTHER        = 99   /* Set via some other mechanism */
    }

    public struct CSVMGlobal
    {
      public static string           name;
      public static CSVMGlobalType   type;           /* Data type */
      public static CSVMGlobalOrigin origin;         /* Default or non-default value */
      public static int      writeable = 1;  /* dynamically writeable */
      public static int      external  = 1;  /* external supported interface */
      public static int      reserved  = 30;
    }

    public struct CSVMExtAttributeInfo
    {
        public static char name;
        public static char type;
        public static char description;
    }

    public abstract class CSVMEnv
    {
      public CSharpVM env;
    }

    public struct DCMDInfo
    {
      char name;                /* Name of the diagnostic command */
      char description;         /* Short description */
      char impact;              /* Impact on the JVM */
      char permission_class;    /* Class name of the required permission if any */
      char permission_name;     /* Permission name of the required permission if any */
      char permission_action;   /* Action name of the required permission if any*/
      int         num_arguments;       /* Number of supported options or arguments */
      bool    enabled;             /* True if the diagnostic command can be invoked, false otherwise*/
    }

    public struct DCMDArgInfo
    {
        public static char name;                /* Option/Argument name*/
        public static char description;         /* Short description */
        public static char type;                /* Type: STRING, BOOLEAN, etc. */
        public static char default_string;      /* Default value in a parsable string */
        public static bool       mandatory;           /* True if the option/argument is mandatory */
        public static bool       option;              /* True if it is an option, false if it is an argument */                                 /* (see diagnosticFramework.hpp for option/argument definitions) */
        public static bool       multiple;            /* True is the option can be specified several time */
        public static int        position;            /* Expected position for this argument (this field is */
                                                      /* meaningless for options) */
    }

    public interface CSVMInterface
    {
      void Reserved1();
      void Reserved2();
      int GetVersion(CSVMEnv env);
      int GetOptionalSupport(CSVMEnv env);
      int GetThreadInfo(CSVMEnv env, long[] ids, int maxDepth, object[] infoArray);
      object[] GetMemoryPools(CSVMEnv env, object mgr);
      object[] GetMemoryManagers(CSVMEnv env, object pool);
      object GetMemoryPoolUsage(CSVMEnv env, object pool);
      object GetPeakMemoryPoolUsage(CSVMEnv env, object pool);
      long GetTotalThreadAllocatedMemory(CSVMEnv env);
      long GetOneThreadAllocatedMemory(CSVMEnv env, long thread_id);
      void GetThreadAllocatedMemory(CSVMEnv env, long[] ids, long[] sizeArray);
      object GetMemoryUsage(CSVMEnv env, bool heap);
      long GetLongAttribute(CSVMEnv env, object obj, CSVMLongAttribute att);
      bool GetBoolAttribute(CSVMEnv env, CSVMBoolAttribute att);
      bool SetBoolAttribute(CSVMEnv env, CSVMBoolAttribute att, bool flag);
      int GetLongAttributes(CSVMEnv env, object obj, CSVMLongAttribute atts, int count, long result);
      object[] FindCircularBlockedThreads(CSVMEnv env);
      long GetThreadCpuTime(CSVMEnv env, long thread_id);
      object[] GetVMGlobalNames(CSVMEnv env);
      int GetVMGlobals(CSVMEnv env, object[] names, CSVMGlobal globals, int count);
      int GetInternalThreadTimes(CSVMEnv env, object[] names, long[] type);
      void SetPoolSensor(CSVMEnv env, object pool, CSVMThresholdType type, object sensor);
      long SetPoolThreshold(CSVMEnv env, object pool, CSVMThresholdType type, long threshold);
      object GetPoolCollectionUsage(CSVMEnv env, object pool);
      int getGCExtAttributeInfo(CSVMEnv env, object mgr, CSVMExtAttributeInfo ext_info, int count);
      long GetThreadCpuTimeWithKind(CSVMEnv env, long[] thread_id, bool user_sys_cpu_time);
      void GetThreadCpuTimeWithKind(CSVMEnv env, long[] ids, long[] timeArray, bool user_sys_cpu_time);
      int DumpHeap0(CSVMEnv env, string outputfile, bool live);
      object[] FindDeadlocks(CSVMEnv env, bool object_monitors_only);
      void SetVMGlobal(CSVMEnv env, string flag_name, object new_value);
      void Reserved6();
      object[] DumpThreads(CSVMEnv env, long[] ids, bool lockedMonitors, bool lockedSynchonizers, int maxDepth);
      void SetGCNotificationEnabled(CSVMEnv env, object mgr, bool enabled);
      object[] GetDiagnosticCommands(CSVMEnv env);
      void GetDiagnosticCommandInfo(CSVMEnv env, object[] cmds, DCMDInfo infoArray);
      string GetDiagnosticCommandArgumentsInfo(CSVMEnv env, object[] cmds, DCMDInfo infoArray, int count);
      string ExecuteDiagnosticCommand(CSVMEnv env, string command);
      void SetDiagnosticFrameworkNotificationEnabled(CSVMEnv env, bool enabled);
    }

    public struct CDSFileMapRegion {
      int     _crc;               // CRC checksum of this region.
      int     _read_only;         // read only region?
      int     _allow_exec;        // executable code in this region?
      int     _is_heap_region;    // Used by SA and debug build.
      int     _is_bitmap_region;  // Relocation bitmap for RO/RW regions (used by SA and debug build).
      int     _mapped_from_file;  // Is this region mapped from a file?
                                  // If false, this region was initialized using ::read().
      int  _file_offset;       // Data for this region starts at this offset in the archive file.
      int  _mapping_offset;    // This encodes the requested address for this region to be mapped at runtime.
                                  // However, the JVM may choose to map at an alternative location (e.g., for ASLR,
                                  // or to adapt to the available ranges in the Java heap range).
                                  // - For an RO/RW region, the requested address is:
                                  //     FileMapHeader::requested_base_address() + _mapping_offset
                                  // - For a heap region, the requested address is:
                                  //     +UseCompressedOops: /*runtime*/ CompressedOops::base() + _mapping_offset
                                  //     -UseCompressedOops: FileMapHeader::heap_begin() + _mapping_offset
                                  //     See FileMapInfo::heap_region_requested_address().
                                  // - For bitmap regions, the _mapping_offset is always zero. The runtime address
                                  //   is picked by the OS.
      int  _used;              // Number of bytes actually used by this region (excluding padding bytes added
                                  // for alignment purposed.
      int  _oopmap_offset;     // Bitmap for relocating oop fields in archived heap objects.
                                  // (The base address is the bottom of the BM region)
      int  _oopmap_size_in_bits;
      int  _ptrmap_offset;     // Bitmap for relocating native pointer fields in archived heap objects.
                                  // (The base address is the bottom of the BM region).
      int  _ptrmap_size_in_bits;
      char   _mapped_base;       // Actually mapped address (NULL if this region is not mapped).
    }

    public struct GenericCDSFileMapHeader {
      int _magic;                    // identification of file type
      int          _crc;                      // header crc checksum, start from _base_archive_name_offset
      int          _version;                  // CURRENT_CDS_ARCHIVE_VERSION of the jdk that dumped the this archive
      int _header_size;              // total size of the header, in bytes
      int _base_archive_name_offset; // offset where the base archive name is stored
                                              //   static archive:  0
                                              //   dynamic archive:
                                              //     0 for default base archive
                                              //     non-zero for non-default base archive
                                              //       (char*)this + _base_archive_name_offset
                                              //       points to a 0-terminated string for the base archive name
      int _base_archive_name_size;   // size of base archive name including ending '\0'
                                              //   static:  0
                                              //   dynamic:
                                              //     0 for default base archive
                                              //     non-zero for non-default base archive
    }

    public struct CDSFileMapHeaderBase {
      // We cannot inherit from GenericCDSFileMapHeader as this type may be used
      // by both C and C++ code.
      GenericCDSFileMapHeader _generic_header;
      CDSFileMapRegion[] _regions;
    }

    public enum CDS 
    {
      NUM_CDS_REGIONS = 4,
      CDS_GENERIC_HEADER_SUPPORTED_MIN_VERSION = 13,
      CURRENT_CDS_ARCHIVE_VERSION = 18
    }

    public struct VMStartType
    {
        public static int DEBUG = 0x0000001;
        public static int RUN_DLL = 0x0000002;
        public static int RUN_EXE = 0x0000003;
        public static int RUN_MODULE = 0x0000004;
        public static int RUN_DRV = 0x0000005;
        public static int RUN_CPL = 0x0000006;
    }

    public struct VMStartArguments
    {
        public static string minMemory = "-XmXB";
        public static string maxMemory = "-XmXL";
        public static string fromDebugMsg = "-debug";
        public static string encodeTag = "-Dfile.encoding.utf-8";
    }

    public class VMDebugMessages
    {
        public static void log(string level, string msg)
        {
            Console.WriteLine(level + msg);
        }
    }

    internal class ModuleUninitializer : Stack
    {
        private static object @lock = new object();
        internal static ModuleUninitializer _ModuleUninitializer = new ModuleUninitializer();
    
        [SecuritySafeCritical]
        internal void AddHandler(EventHandler handler)
        {
          bool lockTaken = false;
          RuntimeHelpers.PrepareConstrainedRegions();
          try
          {
            RuntimeHelpers.PrepareConstrainedRegions();
            Monitor.Enter(ModuleUninitializer.@lock, ref lockTaken);
            RuntimeHelpers.PrepareDelegate((Delegate) handler);
            this.Push((object) handler);
          }
          finally
          {
            if (lockTaken)
              Monitor.Exit(ModuleUninitializer.@lock);
          }
        }
    
        [SecurityCritical]
        static ModuleUninitializer()
        {
        }
    
        [SecuritySafeCritical]
        private ModuleUninitializer()
        {
          EventHandler eventHandler = new EventHandler(this.SingletonDomainUnload);
          AppDomain.CurrentDomain.DomainUnload += eventHandler;
          AppDomain.CurrentDomain.ProcessExit += eventHandler;
        }
    
        [SecurityCritical]
        [PrePrepareMethod]
        private void SingletonDomainUnload(object source, EventArgs arguments)
        {
          bool lockTaken = false;
          RuntimeHelpers.PrepareConstrainedRegions();
          try
          {
            RuntimeHelpers.PrepareConstrainedRegions();
            Monitor.Enter(ModuleUninitializer.@lock, ref lockTaken);
            foreach (EventHandler eventHandler in (Stack) this)
              eventHandler(source, arguments);
          }
          finally
          {
            if (lockTaken)
              Monitor.Exit(ModuleUninitializer.@lock);
          }
        }
    }

    [NativeCppClass]
    [StructLayout(LayoutKind.Sequential, Size = 1)]
    internal struct ThisModule
    {
    }

    [Serializable]
    internal class ModuleLoadException : Exception
    {
      public const string Nested = "A nested exception occurred after the primary exception that caused the C++ module to fail to load.\n";

      protected ModuleLoadException(SerializationInfo info, StreamingContext context)
        : base(info, context)
      {
      }

      public ModuleLoadException(string message, Exception innerException)
        : base(message, innerException)
      {
      }

      public ModuleLoadException(string message)
        : base(message)
      {
      }
    }

    [NativeCppClass]
    [StructLayout(LayoutKind.Sequential, Size = 4)]
    internal struct LanguageSupport
    {
    }

    [DataContract]
    internal class CompilerInput
    {
        [DataMember]
        private readonly WorkflowCompilerParameters parameters;
        [DataMember]
        private readonly string[] files;
    
        public CompilerInput(WorkflowCompilerParameters parameters, string[] files)
        {
          this.parameters = parameters;
          this.files = files;
        }
    
        public WorkflowCompilerParameters Parameters
        {
            get { return this.parameters; }
        }
    
        public string[] Files
        {
            get { return this.files; }
        }
    }

    [Serializable]
    internal struct SerializableMemberAttributes
    {
        private int memberAttributes;
  
        public SerializableMemberAttributes(MemberAttributes memberAttributes)
        {
            this.memberAttributes = Convert.ToInt32((object) memberAttributes);
        }

        public MemberAttributes ToMemberAttributes()
        {
            return (MemberAttributes) this.memberAttributes;
        }
    }

    [GeneratedCode("Microsoft.Build.Tasks.StronglyTypedResourceBuilder", "4.0.0.0")]
    [DebuggerNonUserCode]
    [CompilerGenerated]
    internal class Strings
    {
      private static ResourceManager resourceMan;
      private static CultureInfo resourceCulture;
  
      internal Strings()
      {
      }
  
      [EditorBrowsable(EditorBrowsableState.Advanced)]
      internal static ResourceManager ResourceManager
      {
        get
        {
          if (Strings.resourceMan == null)
            Strings.resourceMan = new ResourceManager("System.Tools.Strings", typeof (Strings).Assembly);
          return Strings.resourceMan;
        }
      }
  
      [EditorBrowsable(EditorBrowsableState.Advanced)]
      internal static CultureInfo Culture
      {
        get { return Strings.resourceCulture; }
        set { Strings.resourceCulture = value; }
      }
  
      internal static string Done = Strings.ResourceManager.GetString("Done", Strings.resourceCulture);
  
      internal static string Error1 = Strings.ResourceManager.GetString("Error1", Strings.resourceCulture);
  
      internal static string Error2 = Strings.ResourceManager.GetString("Error2", Strings.resourceCulture);
  
      internal static string MustNotSpecifyBothPipelineOrAddInRoot = Strings.ResourceManager.GetString("MustNotSpecifyBothPipelineOrAddInRoot", Strings.resourceCulture);
  
      internal static string MustSpecifyPipelineOrAddInRoot = Strings.ResourceManager.GetString("MustSpecifyPipelineOrAddInRoot", Strings.resourceCulture);
  
      internal static string PathInvalid = Strings.ResourceManager.GetString("PathInvalid", Strings.resourceCulture);
  
      internal static string UnknownParameter = Strings.ResourceManager.GetString("UnknownParameter", Strings.resourceCulture);
  
      internal static string Usage = Strings.ResourceManager.GetString("Usage", Strings.resourceCulture);
  
      internal static string Warning = Strings.ResourceManager.GetString("Warning", Strings.resourceCulture);
    }

    public class VMProcessAdding
    {
        [LoaderOptimization(LoaderOptimization.MultiDomainHost)]
        private static int Start(string[] args)
        {
          if (args == null || args.Length != 2 || !args[0].StartsWith("/guid:", StringComparison.Ordinal) || !args[1].StartsWith("/pid:", StringComparison.Ordinal))
            return 1;
          string str = args[0].Remove(0, 6);
          if (str.Length != 36)
            return 1;
          int int32 = Convert.ToInt32(args[1].Remove(0, 5), (IFormatProvider) CultureInfo.InvariantCulture);
          BinaryServerFormatterSinkProvider server = new BinaryServerFormatterSinkProvider();
          server.TypeFilterLevel = TypeFilterLevel.Full;
          BinaryClientFormatterSinkProvider client = new BinaryClientFormatterSinkProvider();
          IDictionary props = (IDictionary) new Hashtable();
          props[(object) "name"] = (object) "ServerChannel";
          props[(object) "portName"] = (object) str;
          props[(object) "typeFilterLevel"] = (object) "Full";
          ChannelServices.RegisterChannel((IChannel) new AddInIpcChannel(props, (IClientChannelSinkProvider) client, (IServerChannelSinkProvider) server), false);
          EventWaitHandle eventWaitHandle = new EventWaitHandle(false, EventResetMode.ManualReset, "AddInProcess:" + str);
          eventWaitHandle.Set();
          eventWaitHandle.Close();
          try
          {
            Process.GetProcessById(int32).WaitForExit();
          }
          catch
          {
          }
          Environment.Exit(0);
          return 0;
        }
    }



    internal class AddInBinaryServerSink : IServerChannelSink, IChannelSinkBase
    {
      private IServerChannelSink _sink;
  
      public AddInBinaryServerSink(IServerChannelSink sink) {
        this._sink = sink;
      }
  
      [SecurityCritical]
      public void AsyncProcessResponse(
        IServerResponseChannelSinkStack sinkStack,
        object state,
        IMessage msg,
        ITransportHeaders headers,
        Stream stream)
      {
        this._sink.AsyncProcessResponse(sinkStack, state, msg, headers, stream);
      }
  
      [SecurityCritical]
      public Stream GetResponseStream(
        IServerResponseChannelSinkStack sinkStack,
        object state,
        IMessage msg,
        ITransportHeaders headers)
      {
        return this._sink.GetResponseStream(sinkStack, state, msg, headers);
      }
  
      public IServerChannelSink NextChannelSink
      {
        [SecurityCritical] get { return this._sink.NextChannelSink; }
      }
  
      [SecurityCritical]
      public ServerProcessing ProcessMessage(
        IServerChannelSinkStack sinkStack,
        IMessage requestMsg,
        ITransportHeaders requestHeaders,
        Stream requestStream,
        out IMessage responseMsg,
        out ITransportHeaders responseHeaders,
        out Stream responseStream)
      {
        new SecurityPermission(SecurityPermissionFlag.UnmanagedCode | SecurityPermissionFlag.SerializationFormatter).Assert();
        return this._sink.ProcessMessage(sinkStack, requestMsg, requestHeaders, requestStream, out responseMsg, out responseHeaders, out responseStream);
      }
  
      public IDictionary Properties
      {
        [SecurityCritical] get { return this._sink.Properties; }
      }
    }

    internal class AddInIpcChannel : IpcChannel
    {
      [SecurityCritical]
      public AddInIpcChannel(
        IDictionary props,
        IClientChannelSinkProvider client,
        IServerChannelSinkProvider server)
        : base(props, (IClientChannelSinkProvider) new AddInBinaryClientFormaterSinkProvider(client), (IServerChannelSinkProvider) new AddInBinaryServerFormaterSinkProvider(server))
      {
      }
    }

    internal class AddInBinaryClientFormaterSinkProvider : IClientChannelSinkProvider
    {
      private IClientChannelSinkProvider _provider;
  
      public AddInBinaryClientFormaterSinkProvider(IClientChannelSinkProvider provider) {
         this._provider = provider;
      }
  
      [SecurityCritical]
      public IClientChannelSink CreateSink(
        IChannelSender channel,
        string url,
        object remoteChannelData)
      {
        return (IClientChannelSink) new AddInBinaryClientFormaterSink(this._provider.CreateSink(channel, url, remoteChannelData));
      }
  
      public IClientChannelSinkProvider Next
      {
        [SecurityCritical] get { return this._provider.Next; }
        [SecurityCritical] set { this._provider.Next = value; }
      }
    }

    internal class AddInBinaryServerFormaterSinkProvider : IServerChannelSinkProvider
    {
      internal IServerChannelSinkProvider _sinkProvider;
  
      public AddInBinaryServerFormaterSinkProvider(IServerChannelSinkProvider sink) {
        this._sinkProvider = sink;
      }
  
      [SecurityCritical]
      public IServerChannelSink CreateSink(IChannelReceiver channel) {
        return (IServerChannelSink) new AddInBinaryServerSink(this._sinkProvider.CreateSink(channel));
      }
  
      [SecurityCritical]
      public void GetChannelData(IChannelDataStore channelData)
      {
        this._sinkProvider.GetChannelData(channelData);
      }

      public IServerChannelSinkProvider Next
      {
        [SecurityCritical] get { return this._sinkProvider.Next; }
        [SecurityCritical] set { this._sinkProvider.Next = value; }
      }
    }

    internal class AddInBinaryClientFormaterSink : IClientChannelSink, IChannelSinkBase, IMessageSink
    {
      private IClientChannelSink _sink;
      private IMessageSink _mSink;
  
      public AddInBinaryClientFormaterSink(IClientChannelSink sink)
      {
        this._sink = sink;
        this._mSink = (IMessageSink) sink;
      }
  
      [SecurityCritical]
      public void AsyncProcessRequest(
        IClientChannelSinkStack sinkStack,
        IMessage msg,
        ITransportHeaders headers,
        Stream stream)
      {
        this._sink.AsyncProcessRequest(sinkStack, msg, headers, stream);
      }
  
      [SecurityCritical]
      public void AsyncProcessResponse(
        IClientResponseChannelSinkStack sinkStack,
        object state,
        ITransportHeaders headers,
        Stream stream)
      {
        this._sink.AsyncProcessResponse(sinkStack, state, headers, stream);
      }
  
      [SecurityCritical]
      public Stream GetRequestStream(IMessage msg, ITransportHeaders headers) 
      { 
        return this._sink.GetRequestStream(msg, headers);
      }

      public IClientChannelSink NextChannelSink
      {
        [SecurityCritical] get { return this._sink.NextChannelSink; }
      }
  
      [SecurityCritical]
      public void ProcessMessage(
        IMessage msg,
        ITransportHeaders requestHeaders,
        Stream requestStream,
        out ITransportHeaders responseHeaders,
        out Stream responseStream)
      {
        this._sink.ProcessMessage(msg, requestHeaders, requestStream, out responseHeaders, out responseStream);
      }
  
      public IDictionary Properties
      {
        [SecurityCritical] get { return this._sink.Properties; }
      }
  
      [SecurityCritical]
      public IMessageCtrl AsyncProcessMessage(IMessage msg, IMessageSink replySink) { return this._mSink.AsyncProcessMessage(msg, replySink); }
  
      public IMessageSink NextSink
      {
        [SecurityCritical] get { return this._mSink.NextSink; }
      }
  
      [SecurityCritical]
      public IMessage SyncProcessMessage(IMessage msg)
      {
        new SecurityPermission(SecurityPermissionFlag.UnmanagedCode | SecurityPermissionFlag.SerializationFormatter).Assert();
        return this._mSink.SyncProcessMessage(msg);
      }
    }

    public struct VMMessageType
    {
        public static string info = "[CSharpVirtualMachine/INFO]: ";
        public static string debug = "[CSharpVirtualMachine/DEBUG]: ";
        public static string error = "[CSharpVirtualMachine/ERROR]: ";
        public static string warn = "[CSharpVirtualMachine/WARNING]: ";
        public static string trace = "[CSharpVirtualMachine/TRACE]: ";
    }
}